# Drawing app

## What is this application about?

This is a simple drawing application using react with canvas and webGl shaders.

Simply it allows for drawing a two dimensional polygon and moving that polygon all around the screen with the ability to
zoom in and out.

## How to start the app?

To start the app simply clone this project run the following commands.

### `npm install` to install all required libraries.

### `npm run start` to start the app on port 8000.
