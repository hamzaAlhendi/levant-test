import React, { useRef, useEffect, useState } from "react";
import { loadShader } from "../utils/webglUtils";

const Canvas = () => {
  const canvasRef = useRef(null);
  const [vertices, setVertices] = useState([]);
  const [isClosed, setIsClosed] = useState(false);
  const [isDragging, setIsDragging] = useState(false);
  const [dragStart, setDragStart] = useState({ x: 0, y: 0 });
  const [zoomLevel, setZoomLevel] = useState(1.0);

  // Vertex and Fragment shader sources
  const vsSource = `
    attribute vec4 aVertexPosition;
    void main() {
      gl_Position = aVertexPosition;
    }
  `;
  const fsSource = `
    precision mediump float;
    void main() {
      gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0); // White color
    }
  `;

  //   To check whether the new point clicked is inside the polygon or not
  const isPointInsidePolygon = (x, y, vertices) => {
    let inside = false;
    for (
      let i = 0, j = vertices.length / 2 - 1;
      i < vertices.length / 2;
      j = i++
    ) {
      let xi = vertices[i * 2],
        yi = vertices[i * 2 + 1];
      let xj = vertices[j * 2],
        yj = vertices[j * 2 + 1];

      let intersect =
        yi > y !== yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
      if (intersect) inside = !inside;
    }
    return inside;
  };

  //   To convert from canvas to webgl
  const screenToWebGL = (x, y, canvas) => {
    return [
      (x / canvas.clientWidth) * 2 - 1, // Convert x from [0, canvas.width] to [-1, 1]
      -((y / canvas.clientHeight) * 2 - 1), // Convert y from [0, canvas.height] to [-1, 1] and invert y-axis
    ];
  };

  function clamp(value, min, max) {
    return Math.max(min, Math.min(max, value));
  }

  // rerender the scene every time the vertices (array of vertex) or the isClosed variable change
  useEffect(() => {
    const canvas = canvasRef.current;
    const gl = canvas.getContext("webgl");

    if (!gl) {
      console.error("Unable to initialize WebGL.");
      return;
    }

    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);
    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      console.error(
        "Unable to initialize the shader program:",
        gl.getProgramInfoLog(shaderProgram)
      );
      return;
    }

    gl.clearColor(1.0, 1.0, 1.0, 1.0); // Clear to white
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.useProgram(shaderProgram);
    gl.lineWidth(1.0);

    if (vertices.length === 0) return;

    const buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

    const vertexPosition = gl.getAttribLocation(
      shaderProgram,
      "aVertexPosition"
    );
    gl.enableVertexAttribArray(vertexPosition);
    gl.vertexAttribPointer(vertexPosition, 2, gl.FLOAT, false, 0, 0);

    const mode = isClosed ? gl.LINE_LOOP : gl.LINE_STRIP;
    gl.drawArrays(mode, 0, vertices.length / 2);
  }, [vertices, isClosed]);

  const handleMouseDown = (event) => {
    const canvas = canvasRef.current;
    const rect = canvas.getBoundingClientRect();
    const [x, y] = screenToWebGL(
      event.clientX - rect.left,
      event.clientY - rect.top,
      canvas
    );
    // when the polygon is already closed we are either dragging or want to create a new polygon
    if (isClosed) {
      if (isPointInsidePolygon(x, y, vertices)) {
        setIsDragging(true);
        setDragStart({ x, y });
      } else {
        setIsDragging(false);
        setIsClosed(false);
        setVertices([x, y]);
      }
    }
    //  when not closed check if it might be closed when the new point is added
    else if (
      vertices.length >= 2 &&
      Math.hypot(vertices[0] - x, vertices[1] - y) < 0.1
    ) {
      setIsClosed(true);
    } else {
      setVertices([...vertices, x, y]); // Add vertex
    }
  };

  const handleMouseMove = (event) => {
    if (!isDragging) return;

    const canvas = canvasRef.current;
    const rect = canvas.getBoundingClientRect();
    const [currentX, currentY] = screenToWebGL(
      event.clientX - rect.left,
      event.clientY - rect.top,
      canvas
    );

    const dx = currentX - dragStart.x;
    const dy = currentY - dragStart.y;

    // Prepare to adjust vertices without letting them go out of bounds
    let minX = 1,
      maxX = -1,
      minY = 1,
      maxY = -1;

    // Calculate new tentative vertices to find boundaries
    const newVertices = vertices.map((v, index) => {
      if (index % 2 === 0) {
        minX = Math.min(minX, v + dx);
        maxX = Math.max(maxX, v + dx);
        return v + dx;
      } else {
        minY = Math.min(minY, v + dy);
        maxY = Math.max(maxY, v + dy);
        return v + dy;
      }
    });

    // Check if the new vertices would go out of the WebGL viewport
    const overflowX = Math.max(0, maxX - 1, -minX - 1);
    const overflowY = Math.max(0, maxY - 1, -minY - 1);

    // Adjust vertices, clamping them within the bounds
    const clampedVertices = newVertices.map((v, index) => {
      if (index % 2 === 0) {
        // X coordinate
        return clamp(v - overflowX * Math.sign(dx), -1, 1);
      } else {
        // Y coordinate
        return clamp(v - overflowY * Math.sign(dy), -1, 1);
      }
    });

    setVertices(clampedVertices);
    setDragStart({ x: currentX, y: currentY });
  };

  const handleWheel = (event) => {
    const canvas = canvasRef.current;
    const rect = canvas.getBoundingClientRect();
    const [x, y] = screenToWebGL(
      event.clientX - rect.left,
      event.clientY - rect.top,
      canvas
    );

    if (isPointInsidePolygon(x, y, vertices) && isClosed) {
      event.preventDefault(); // Prevent the page from scrolling
      const zoomFactor = 0.05; // Control the rate of zoom
      const direction = event.deltaY > 0 ? -1 : 1; // Determine zoom direction
      const newZoomLevel = clamp(zoomLevel + direction * zoomFactor, 0.1, 10); // Apply and clamp new zoom level

      // Calculate the new vertices
      const scale = newZoomLevel / zoomLevel;
      const newVertices = vertices.map((v, index) => {
        const center = index % 2 === 0 ? x : y;
        return (v - center) * scale + center; // Scale around the cursor position
      });

      setZoomLevel(newZoomLevel);
      setVertices(newVertices);
    }
  };

  const handleMouseUp = () => {
    setIsDragging(false);
  };

  return (
    <div style={{ width: "100%", height: "100vh", overflow: "hidden" }}>
      <canvas
        ref={canvasRef}
        onWheel={handleWheel}
        onMouseDown={handleMouseDown}
        onMouseMove={handleMouseMove}
        onMouseUp={handleMouseUp}
        style={{ width: "100%", height: "100%" }}
      />
    </div>
  );
};

export default Canvas;
